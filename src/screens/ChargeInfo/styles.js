import { StyleSheet } from "aphrodite";

const styles = StyleSheet.create({
  ChargeInfoWrapper: {
    display: "flex",
    flexWrap: "wrap",
    width: "100%",
    justifyContent: "center",
  },
});

export default styles;
