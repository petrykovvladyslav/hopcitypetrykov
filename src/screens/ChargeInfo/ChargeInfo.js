import { css } from "aphrodite";
import Item from "../../components/Item/Item";

import styles from "./styles";

const ChargeInfo = (props) => {
  const { items } = props;

  if (!items) return <p>Loading...</p>;

  return (
    <div className={css(styles.ChargeInfoWrapper)}>
      {items.map((item, index) => {
        const { id, name, battery_status } = item;
        return (
          <Item
            id={id}
            key={index}
            name={name}
            index={index}
            battery_status={battery_status}
          />
        );
      })}
    </div>
  );
};

export default ChargeInfo;
