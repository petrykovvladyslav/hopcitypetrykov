import { css } from "aphrodite";
import skuter from "../../assets/images/skuter.svg";

import styles from "./styles";

const Item = (props) => {
  const { id, name, index, battery_status } = props;
  const { percentage_level } = battery_status;

  const getPecentageColor = (status) => {
    let colorToReturn = "#FF0000C7";

    if (status >= 50) {
      colorToReturn = "#A1FF00";
    } else if (status > 30 && status < 50) {
      colorToReturn = "#FFFF00D6";
    }

    return colorToReturn;
  };

  return (
    <div key={index} className={css(styles.ItemWrapper)}>
      <img alt={id} src={skuter} width={150} height={150} />
      <p>{name}</p>
      <p>{percentage_level}</p>
      <div
        className={css(styles.PercentageLevel)}
        style={{
          backgroundColor: getPecentageColor(percentage_level),
        }}
      />
    </div>
  );
};

export default Item;
