import { StyleSheet } from "aphrodite";

const styles = StyleSheet.create({
  ItemWrapper: {
    width: "10%",
    "@media (max-width: 1400px)": {
      width: "30%",
    },
    "@media (max-width: 1000px)": {
      width: "50%",
    },
    padding: "5px",
    margin: "10px",
    cursor: "pointer",
    textAlign: "center",
    borderRadius: "5px",
    border: "1px solid #00000045",
    boxShadow: "5px 5px 15px 5px rgba(0,0,0,0.15)",
  },
  PercentageLevel: {
    width: "100%",
    height: "15px",
  },
});

export default styles;
