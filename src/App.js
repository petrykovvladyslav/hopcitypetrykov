import { useEffect, useState } from "react";
import { w3cwebsocket as W3CWebSocket } from "websocket";
import Dropdown from "react-dropdown";
import { css } from "aphrodite";

import "react-dropdown/style.css";
import ChargeInfo from "./screens/ChargeInfo/ChargeInfo";

import styles from "./styles";

const client = new W3CWebSocket("wss://api.hop.city/v1/ws");
const url = { name: "vehicle/view/rentable/subscribe" };

const groupsOptions = ["gliwice", "konin", "rdlabs", "all"];

const App = () => {
  const [items, setItems] = useState([]);
  const [filteredData, setFilteredData] = useState([]);
  const [dropdownValue, setDropdownValue] = useState("all");
  const [webSocketStatus, setWebSocketStatus] = useState(false);

  useEffect(() => {
    client.onopen = () => {
      client.send(JSON.stringify(url));
      setWebSocketStatus(true);
    };

    client.onmessage = (message) => {
      const { data } = JSON.parse(message.data);

      if (data !== null) {
        data.forEach((newItem) => {
          const { _change } = newItem;

          switch (_change) {
            case "add": {
              setItems((array) => [...array, newItem]);
              break;
            }
            case "update": {
              let itemIndex = null;
              items.map((item, index) => {
                if (item.id === newItem.id) {
                  return (itemIndex = index);
                }
                return (itemIndex = null);
              });

              if (!itemIndex) {
                setItems((array) => [...array, newItem]);
              } else {
                let updatedArray = [...items];
                updatedArray[itemIndex] = newItem;
                setItems(updatedArray);
              }
              break;
            }
            case "remove": {
              setItems(items.filter((item) => item.id !== newItem.id));
              break;
            }
            default: {
              console.error("None of switch statements were executed");
              break;
            }
          }
        });
      }
    };

    client.onerror = (err) => {
      console.error(
        "Socket encountered error: ",
        err.message,
        "Closing socket"
      );
      client.close();
    };

    client.onclose = (message) => {
      console.log(
        "Socket is closed. Reconnect will be attempted in 3 second.",
        message.reason
      );
      setTimeout(function () {
        client.send(JSON.stringify(url));
      }, 3000);
    };
  }, [items]);

  useEffect(() => {
    if (dropdownValue === "all") {
      setFilteredData([...items]);
    } else {
      const filteredArray = [...items].filter(
        (item) => item.group === dropdownValue
      );
      setFilteredData(filteredArray);
    }
  }, [dropdownValue, items]);

  return (
    <div className={css(styles.ApplicationWrapper)}>
      <p>WebSocket connection is {!webSocketStatus && "not"} established</p>
      <p>There are {filteredData.length} items available in this group</p>
      <Dropdown
        options={groupsOptions}
        onChange={(item) => {
          const { value } = item;
          setDropdownValue(value);
        }}
        value={dropdownValue}
        placeholder="Select to filter by group..."
      />
      {!filteredData.length ? (
        <p>There are no scooters in group {dropdownValue}</p>
      ) : (
        <ChargeInfo items={filteredData} />
      )}
    </div>
  );
};

export default App;
