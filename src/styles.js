import { StyleSheet } from "aphrodite";

const styles = StyleSheet.create({
  ApplicationWrapper: {
    width: "100%",
    display: "flex",
    flexWrap: "wrap",
    alignItems: "center",
    flexDirection: "column",
    justifyContent: "center",
  },
});

export default styles;
