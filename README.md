Possible improvements I am usually doing for the projects:

1. Adding prop types validation for every component 
2. Including an eslint for linting the code (usually following airbnb code standards)
3. Creating pipelines on git-based control systems (GitLab, Bitbucket, GitHub)
4. Using feature branching technology to separate each functionality on different working branches 
5. Merging reviewed branches into master
6. Using globally defined settings for styling, max/min screen values parameters (mobile/tablet/desktop)
7. Creating a possibility of using an app by selecting given languages (eng/ukr/pln)
10. Create a separate routing file to include components with the related paths
    - make use of this file in App.js file 
11. Global imports from the defined directory to avoid imports like "../"

To run app use following commands:
1. npm i
2. npm run start
